#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <ostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <chrono>
#include <thread>

#define GRID_BLANK ' '
#define GRID_CROSS '-'
#define GRID_MARK '#'
#define NUM_WIDTH 3

/*

TODO:
Let's approach this from a bitfield standpoint. It's basically a bunch of bitwise operations.
For each possibility:
	AND it with what's already there to make sure it doesn't overwrite anything.
	NAND it with the crosses already there to make sure it doesn't overwrite any crosses.
Take all valid possiblities and AND them together to fill in the bits we're certain about.

*/

static bool isnum(char c)
{
	return c >= '0' && c <= '9';
}

static std::string binary(unsigned int line, int size)
{
	std::stringstream str;
	for (int i = 0; i < size; ++i)
	{
		str << (line & (1 << i) ? '1' : '0');
	}
	return str.str();
}

class Nonogram
{
private:
	std::vector<std::vector<int>> columns;
	std::vector<std::vector<int>> rows;
	std::vector<std::vector<char>> grid;
public:
	Nonogram(std::vector<std::string> &column_lines, std::vector<std::string> &row_lines)
	{
		std::vector<int> ones_places;
		char prev_c = ' ';
		for (int i = 0; i < column_lines.back().size(); ++i)
		{
			char c = column_lines.back()[i];

			if (!isnum(c) && isnum(prev_c))
			{
				// was a number previously, but no longer
				ones_places.push_back(i);
			}
			prev_c = c;
		}

		for (int i = 0; i < ones_places.size(); ++i)
		{
			columns.emplace_back();
		}

		for (int i = 0; i < column_lines.size(); ++i)
		{
			std::string &line = column_lines[i];
			int prev_place = 0;
			for (int j = 0; j < ones_places.size(); ++j)
			{
				int place = ones_places[j];
				if (place > line.size())
				{
					break;
				}
				std::stringstream segment(line.substr(prev_place, place - prev_place));

				int num = -1;
				segment >> num;

				if (num != -1)
				{
					columns[j].push_back(num);
				}

				prev_place = place;
			}
		}
		
		for (int i = 0; i < row_lines.size(); ++i)
		{
			rows.emplace_back();
			grid.emplace_back();
			for (int j = 0; j < ones_places.size(); ++j)
			{
				grid[i].push_back(GRID_BLANK);
			}
			std::stringstream stream(row_lines[i]);
			while (true)
			{
				int num = -1;
				stream >> num;
				if (num == -1)
					break;
				rows[i].push_back(num);
			}
		}
	};

	friend std::ostream& operator<<(std::ostream &stream, Nonogram &gram)
	{
		int row_max = 0;
		for (int i = 0; i < gram.rows.size(); ++i)
		{
			if (gram.rows[i].size() > row_max)
				row_max = gram.rows[i].size();
		}

		int col_max = 0;
		for (int i = 0; i < gram.columns.size(); ++i)
		{
			if (gram.columns[i].size() > col_max)
				col_max = gram.columns[i].size();
		}

		for (int i = 0; i < col_max; ++i)
		{
			stream << std::string(row_max * NUM_WIDTH, ' ');
			for (int j = 0; j < gram.columns.size(); ++j)
			{
				int index = gram.columns[j].size() - col_max + i;
				if (index >= 0)
				{
					stream << std::setw(NUM_WIDTH) << gram.columns[j][index];
				}
				else
				{
					stream << std::string(NUM_WIDTH, ' ');
				}
			}
			stream << '\n';
		}

		for (int i = 0; i < gram.rows.size(); ++i)
		{
			for (int j = 0; j < row_max; ++j)
			{
				int index = gram.rows[i].size() - row_max + j;
				if (index >= 0)
				{
					stream << std::setw(NUM_WIDTH) << gram.rows[i][index];
				}
				else
				{
					stream << std::string(NUM_WIDTH, ' ');
				}
			}
			for (int c = 0; c < gram.columns.size(); ++c)
			{
				stream << std::setw(NUM_WIDTH) << gram.grid[i][c];
			}
			stream << '\n'; 
		}
		
		stream.flush();
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		return stream;
	}

	bool solved()
	{
		for (int row = 0; row < rows.size(); ++row)
		{
			int streak = 0;
			int block = 0;
			for (int col = 0; col < columns.size(); ++col)
			{
				if (grid[row][col] == GRID_MARK)
				{
					++streak;
				}
				else if (streak > 0)
				{
					if (block >= rows[row].size() || streak != rows[row][block])
					{
						return false;
					}
					streak = 0;
					++block;
				}
			}
			if (streak > 0)
			{
				if (block >= rows[row].size() || streak != rows[row][block])
				{
					return false;
				}
				streak = 0;
				++block;
			}
			if (block != rows[row].size())
				return false;
		}
		for (int col = 0; col < columns.size(); ++col)
		{
			int streak = 0;
			int block = 0;
			for (int row = 0; row < rows.size(); ++row)
			{
				if (grid[row][col] == GRID_MARK)
				{
					++streak;
				}
				else if (streak > 0)
				{
					if (block >= columns[col].size() || streak != columns[col][block])
						return false;
					streak = 0;
					++block;
				}
			}
			if (streak > 0)
			{
				if (block >= columns[col].size() || streak != columns[col][block])
					return false;
				streak = 0;
				++block;
			}
			if (block != columns[col].size())
				return false;
		}
		return true;
	}

	unsigned int fill_line(const std::vector<int> &blocks, const std::vector<int> &spaces)
	{
		if (spaces.size() != blocks.size() + 1)
		{
			std::cout << "expected size " << (blocks.size() + 1) << ", got " << spaces.size() << std::endl;
			exit(2);
			return 0;
		}
		unsigned int possibility = 0;
		int index = 0;
		for (int i = 0; i < blocks.size(); ++i)
		{
			index += spaces[i];
			for (int j = 0; j < blocks[i]; ++j)
			{
				possibility |= (1 << index);
				++index;
			}
		}
		return possibility;
	}

	void generate_possibilities(std::vector<unsigned int> &possibilities, int size, std::vector<int> blocks)
	{
		std::vector<int> spaces;
		int index = 0;

		spaces.push_back(0);
		for (int i = 0; i < blocks.size(); ++i)
		{
			index += blocks[i];
			spaces.push_back(1);
			++index;
		}
		--index; // fencepost problem

		spaces.back() = size - index; // fill remaining space

		if (blocks.size() == 1 && !blocks[0])
		{
			spaces.erase(spaces.begin());
			blocks.clear();
		}
		/*
		std::cout << "spaces:";
		for (int space : spaces)
		{
			std::cout << " " << space;
		}
		std::cout << std::endl;
		*/
		// for next iteration of spaces:
		// from the right, check for spaces > 1
		// shrink right space, grow left space if right space > 1 or right space is on end
		// pack everything to the right of the left space as tight as possible
		// #-#-#----- // right 5, left 1
		// #-#--#---- // right 4, left 2
		// ...
		// #-#------# // right 6, left 1
		// #--#-#---- // right 4, left 1
		// #--#--#--- // right 3, left 2
		// ...
		// #--#-----# // right 5, left 2
		// #---#-#--- // right 3, left 1
		// ...
		// #-----#--# // right 2, left 5
		// #------#-# // right 6, left 0
		// -#-#-#---- // right 4, left 1
		// ...
		// ----#--#-# // right 2, left 4
		// -----#-#-# // right 5, no left

		unsigned int def = fill_line(blocks, spaces);
		possibilities.push_back(def);
		while (true)
		{
			loopstart:
			for (int i = spaces.size() - 1; i > 0; --i) // if i gets to 0, we're done here
			{
				if (spaces[i] > 1 || (i == spaces.size() - 1 && spaces[i] == 1))
				{
					// shrink right space
					--spaces[i];
					++spaces[i - 1];
					for (int j = i; j < spaces.size() - 1; ++j) // don't mess with last element
					{
						while (spaces[j] > 1)
						{
							--spaces[j];
							++spaces.back();
						}
					}
					unsigned int possibility = fill_line(blocks, spaces);
					possibilities.push_back(possibility);
					goto loopstart; // start over
				}
			}
			break;
		}
	}

	void solve_line(unsigned long &marked, unsigned long &crossed, int size, std::vector<int> &blocks)
	{
		/*
		std::cout << "solving line: ";
		for (int i = 0; i < size; ++i)
		{
			std::cout << (marked & (1 << i) ? GRID_MARK : (crossed & (1 << i) ? GRID_CROSS : GRID_BLANK));
		}
		std::cout << ";" << std::endl;
		std::cout << "marked: " << binary(marked, size) << std::endl;
		std::cout << "crossed:" << binary(crossed, size) << std::endl << "blocks: ";;
		for (int i = 0; i < blocks.size(); ++i)
		{
			std::cout << blocks[i] << " ";
		}
		std::cout << std::endl << "size: " << size << std::endl;
		*/

		// Generate all possible configurations of the blocks
		std::vector<unsigned int> possibilities;
		generate_possibilities(possibilities, size, blocks);

		possibilities.erase(std::remove_if(possibilities.begin(), possibilities.end(), [&](unsigned int possibility)
		{
			return (marked & ~possibility) || (crossed & possibility); // filter any possibilities that conflict with the marked or crossed
		}), possibilities.end());

		/*
		std::cout << "possibilities[" << possibilities.size() << "]: " << std::endl;
		for (int possibility : possibilities)
		{
			for (int j = 0; j < size; ++j)
			{
				std::cout << (possibility & (1 << j) ? GRID_MARK : GRID_BLANK);
			}
			std::cout << ";" << std::endl;
		}
		*/

		if (!possibilities.empty())
		{
			unsigned int certain = ~crossed;
			for (int p : possibilities)
			{
				certain &= p;
			}

			if (marked & ~certain)
			{
				std::cout << "!!!TRIED TO UNMARK!!!" << std::endl;
				std::cout << "marked:  " << binary(marked, size) << std::endl;
				std::cout << "certain: " << binary(certain, size) << std::endl;
				exit(3);
			}
			marked = certain;

			// 1 2:
			// ----
			// #x---
			// x#x--

			// fill in crosses now
			unsigned int certainly_not = ~0;
			for (int p : possibilities)
			{
				certainly_not &= ~p;
			}

			if (crossed & ~certainly_not)
			{
				std::cout << "!!!TRIED TO UNCROSS!!!" << std::endl;
				std::cout << "crossed:       " << binary(crossed, size) << std::endl;
				std::cout << "certainly_not: " << binary(crossed, size) << std::endl;
				exit(4);
			}
			crossed = certainly_not;

			/*
			std::cout << "marked:";
			for (int j = 0; j < size; ++j)
			{
				std::cout << (marked & (1 << j) ? GRID_MARK : GRID_BLANK);
			}
			std::cout << ";" << std::endl;
			std::cout << "crossed:";
			for (int j = 0; j < size; ++j)
			{
				std::cout << (crossed & (1 << j) ? GRID_CROSS : GRID_BLANK);
			}
			std::cout << ";" << std::endl;
			*/

		}
		//std::cout << std::endl;
	}

	void solve()
	{
		for (int row = 0; row < rows.size(); ++row)
		{
			unsigned long marked = 0;
			unsigned long crossed = 0;
			for (int col = 0; col < columns.size(); ++col)
			{
				if (grid[row][col] == GRID_MARK)
				{
					marked |= (1 << col);
				}
				else if (grid[row][col] == GRID_CROSS)
				{
					crossed |= (1 << col);
				}
			}
			solve_line(marked, crossed, columns.size(), rows[row]);
			for (int col = 0; col < columns.size(); ++col)
			{
				grid[row][col] = marked & (1 << col) ? GRID_MARK : (crossed & (1 << col) ? GRID_CROSS : GRID_BLANK);
			}
			std::cout << *this << std::endl;
		}
		for (int col = 0; col < columns.size(); ++col)
		{
			unsigned long marked = 0;
			unsigned long crossed = 0;
			for (int row = 0; row < rows.size(); ++row)
			{
				if (grid[row][col] == GRID_MARK)
				{
					marked |= (1 << row);
				}
				else if (grid[row][col] == GRID_CROSS)
				{
					crossed |= (1 << row);
				}
			}
			solve_line(marked, crossed, rows.size(), columns[col]);
			for (int row = 0; row < rows.size(); ++row)
			{
				grid[row][col] = marked & (1 << row) ? GRID_MARK : (crossed & (1 << row) ? GRID_CROSS : GRID_BLANK);
			}
			std::cout << *this << std::endl;
		}
	}


};

int main(int argc, char **argv)
{
	// prevent flush on newline char
	std::setvbuf(stdout, nullptr, _IOFBF, BUFSIZ);
	std::cout << "hello world" << '\n';

	if (argc < 2)
	{
		std::cout << "no file specified!" << std::endl;
		return 1;
	}

	std::ifstream file(argv[1]);

	if (!file)
	{
		std::cout << "file not found" << std::endl;
		return 1;
	}


	int prevlen = 0;

	std::vector<std::string> column_lines;
	std::vector<std::string> row_lines;

	for (std::string line; std::getline(file, line); )
	{
		if (row_lines.empty())
		{
			if (prevlen > line.size())
			{
				row_lines.push_back(line);
			}
			else
			{
				column_lines.push_back(line);
			}
		}
		else
		{
			row_lines.push_back(line);
		}
	prevlen = line.size();
	}


	Nonogram nono(column_lines, row_lines);
	while (!nono.solved())
	{
		nono.solve();
	}
	
}
